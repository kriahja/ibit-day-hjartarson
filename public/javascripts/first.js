var elfApp = angular.module('elfApp');

elfApp.controller('FirstController', function($scope, $http) {
    'use strict';
    
   
    $scope.description = 'FirstController Data';
    
    $scope.loadScript = function(){
        $http.get('hello.js').then(function(hello){
            $scope.hello
        })
    }
    
    $scope.loadScript();
    
    $scope.loadData = function() {
        $http.get('Presidents.json').then(function(presidents) {
                $scope.presidents = JSON.stringify(presidents, null, 4);
            })
    };

    $scope.loadData();
     console.log("Hello kristjan controller");

});


elfApp.directive('elfFirstDescription', function() {
    'use strict';
    return {
        controller: 'FirstController',
        templateUrl: 'first-controller'
    };
});

elfApp.directive('elfFirstData', function() {
    'use strict';
    return {
        controller: 'FirstController',
        templateUrl: 'first-data'
    };
});

elfApp.directive('elfFirstScript', function() {
    'use strict';
    return {
        controller: 'FirstController',
        templateUrl: 'first-script'
    };
});