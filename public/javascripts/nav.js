var elfApp = angular.module('elfApp', ['ngMaterial']);

elfApp.controller('navCtrl', ['$scope', '$mdSidenav', function($scope, $mdSidenav){
  $scope.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };
 
}]);