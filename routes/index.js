var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  'use strict';
  res.render('index', {
    title: 'ibit02'
  });
});

router.get('/:id', function(req, res, next) {
  'use strict';
  res.render(req.params.id, {
    title: 'ibit02'
  });
});

// router.get('v_controller/:id', function(request, response, next) {
//   'use strict';
//   response.render('v_controller/' + request.params.id, {
//     title: 'ibit02'
//   });
// });



module.exports = router;
